const User = require('../models/user');

exports.getOne = async function(req, res, next) {
  const user = await User.findOne({_id: req.userId});
  if (!user) {
    res.status(400).send({
      message: 'Not found',
    });
    return;
  }
  try {
    res.send({_id: user._id, username: user.username,
      createdDate: user.createdDate});
  } catch (error) {
    res.status(500).send(error);
  }
};
