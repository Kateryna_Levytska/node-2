const Note = require('../models/note');

exports.getAll = async function(req, res, next) {
  const {offset, limit} = req.query;
  const notes = await Note.find({userId: req.userId});
  try {
    const noteData = notes.splice(offset || 0, limit || notes.length);
    res.send({notes: noteData, offset: Number(offset || 0),
      limit: Number(limit || 0), count: noteData.length});
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.getOne = async function(req, res, next) {
  const {id} = req.params;
  const note = await Note.findOne({_id: id, userId: req.userId});
  if (!note) {
    res.status(400).send({
      message: 'Not found',
    });
    return;
  }
  try {
    res.send(note);
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.changeComplete = async function(req, res, next) {
  const {id} = req.params;
  const note = await Note.findOne({_id: id, userId: req.userId});
  if (!note) {
    res.status(400).send({
      message: 'Not found',
    });
    return;
  }
  try {
    await Note.updateOne({_id: id}, {completed: !note.completed});
    res.send({
      message: 'Success',
    });
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.del = async function(req, res, next) {
  const {id} = req.params;
  try {
    const note = await Note.deleteOne({_id: id, userId: req.userId});
    if (note.deletedCount < 1) {
      res.status(400).send({
        message: 'Deleting error',
      });
      return;
    }
    res.send({
      message: 'Success',
    });
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.create = async function(req, res, next) {
  if (!req.body.text) {
    res.status(400).send({
      message: 'Text is missing',
    });
    return;
  }
  const note = new Note({...req.body, userId: req.userId});
  try {
    await note.save();
    res.send({
      message: 'Success',
    });
  } catch (error) {
    res.status(500).send(error);
  }
};
