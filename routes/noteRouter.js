const express = require('express');
const {getAll, create, getOne, del, changeComplete} =
require('../controllers/noteController.js');
const noteRouter = express.Router();

noteRouter.get('/', getAll);
noteRouter.post('/', create);
noteRouter.get('/:id', getOne);
noteRouter.patch('/:id', changeComplete);
noteRouter.delete('/:id', del);

module.exports = noteRouter;
